meta:
  id: klwa
  endian: be
  license: Unlicense
  file-extension: klwa

seq:
- id: header
  type: file_header
  repeat: until
  repeat-until: _.name == ""
- id: files
  type: file(_index)
  size: header[_index].length
  repeat: expr
  repeat-expr: header.size - 1

types:
  file_header:
    seq:
    - id: name
      encoding: UTF-8
      type: strz
    - id: length
      if: name != ""
      type: u4
  file:
    params:
    - id: i
      type: u4
    seq:
    - id: body
      size-eos: true
    instances:
      name:
        value: _parent.header[i].name
