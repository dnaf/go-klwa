# klwa

klwa is a simple tiny archive format that I made for no real reason. I couldn't
find any existing specifications for archives that fit my specific needs
perfectly, so I figured I might as well define my own. I am fully confident that
there are several near-identical formats to this one floating around, but I
couldn't easily find any.

You probably don't have any reason to use this.

TODO: real documentation