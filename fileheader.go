package klwa

import (
	"encoding/binary"
	"io"
)

type FileHeader struct {
	Name   string
	Size   int
	offset int
}

func readFileHeader(r io.Reader) (*FileHeader, error) {
	name, err := readNullTerminatedString(r)
	if err != nil {
		if err == io.EOF {
			return nil, ErrPrematureEOF
		}
		return nil, err
	}
	if len(name) == 0 {
		return nil, nil
	}

	var size uint32
	if err := binary.Read(r, binary.BigEndian, &size); err != nil {
		if err == io.EOF {
			return nil, ErrPrematureEOF
		}
		return nil, err
	}

	return &FileHeader{
		Name: name,
		Size: int(size),
	}, nil
}

func (hdr FileHeader) write(w io.Writer) error {
	if _, err := w.Write([]byte(hdr.Name)); err != nil {
		return err
	}
	if _, err := w.Write([]byte{0}); err != nil {
		return err
	}
	if err := binary.Write(w, binary.BigEndian, uint32(hdr.Size)); err != nil {
		return err
	}
	return nil
}
