package klwa

import (
	"io"
)

type Writer struct {
	w           io.Writer // underlying writer
	files       []FileHeader
	currentfile int
	filecursor  int
}

// NewWriter creates a new KLWA writer that will write the given files.
func NewWriter(writer io.Writer, files []FileHeader) (*Writer, error) {
	w := &Writer{
		w:           writer,
		files:       files,
		currentfile: -1,
	}

	var archiveSize int
	var headerSize int
	for _, hdr := range w.files {
		if checkStringForNull(hdr.Name) {
			return nil, ErrNullInFilename
		}
		if hdr.Size > 0x100000000 {
			return nil, ErrFileTooBig
		}

		archiveSize += hdr.Size
		headerSize += len(hdr.Name) + 1 + 4 + 4
		if archiveSize+headerSize > 0x100000000 {
			return nil, ErrArchiveTooLarge
		}
	}

	var acc int
	for _, hdr := range w.files {
		hdr.offset = acc + headerSize
		hdr.write(w.w)
		acc += hdr.Size
	}

	if _, err := w.w.Write([]byte{0x00}); err != nil {
		return nil, err
	}

	return w, nil
}

// Next prepares the writer to write the next file. It returns the next files FileHeader.
// If the writer has no more files, Write returns ErrNoFilesLeft.
// If the fewer bytes were written for the previous file than specified by its FileHeader, Write returns ErrWriteTooShort.
func (w *Writer) Next() (*FileHeader, error) {
	if w.currentfile > -1 {
		if w.filecursor < w.files[w.currentfile].Size {
			return nil, ErrWriteTooShort
		}
	}
	w.currentfile++
	w.filecursor = 0
	if w.currentfile >= len(w.files) {
		return nil, ErrNoFilesLeft
	}
	hdr := w.files[w.currentfile]
	return &hdr, nil
}

// Write writes to the current file in the archive.
// If more bytes are written than set in the FileHeader, Write returns ErrWriteTooLong.
// If Next() has not been called, Write returns ErrNextNotCalled.
func (w *Writer) Write(b []byte) (int, error) {
	if w.currentfile == -1 {
		return 0, ErrNextNotCalled
	}
	if w.filecursor+len(b) > w.files[w.currentfile].Size {
		return 0, ErrWriteTooLong
	}
	n, err := w.w.Write(b)
	w.filecursor += n
	return n, err
}
