package klwa

import (
	"io"
	"io/ioutil"
)

type Reader struct {
	Files []FileHeader

	r           io.Reader
	currentfile int
	filecursor  int
}

func NewReader(reader io.Reader) (*Reader, error) {
	r := &Reader{
		r:           reader,
		Files:       make([]FileHeader, 0),
		currentfile: -1,
	}

	for {
		hdr, err := readFileHeader(r.r)
		if err != nil {
			return nil, err
		}
		if hdr == nil {
			break
		}
		r.Files = append(r.Files, *hdr)
	}

	return r, nil
}

// Next prepares the reader to read the next file. It returns the next file's FileHeader.
// If the reader has no more files, Read returns io.EOF.
// If the previous file has not been completely read, it will discard the remaining bytes.
func (r *Reader) Next() (*FileHeader, error) {
	if r.currentfile > -1 {
		n := int64(r.Files[r.currentfile].Size - r.filecursor)
		if sk, ok := r.r.(io.Seeker); ok {
			_, err := sk.Seek(n, io.SeekCurrent)
			if err != nil {
				return nil, err
			}
		} else {
			if _, err := io.CopyN(ioutil.Discard, r.r, n); err != nil {
				if err == io.EOF {
					return nil, ErrPrematureEOF
				}
				return nil, err
			}
		}
	}

	r.currentfile++
	r.filecursor = 0
	if r.currentfile >= len(r.Files) {
		return nil, io.EOF
	}
	hdr := r.Files[r.currentfile]
	return &hdr, nil
}

func (r *Reader) Read(b []byte) (int, error) {
	if r.currentfile == -1 {
		return 0, ErrNextNotCalled
	}
	size := r.Files[r.currentfile].Size
	maxn := len(b)
	if r.filecursor+len(b) > size {
		maxn = size - r.filecursor
	}
	n, err := r.r.Read(b[:maxn])
	r.filecursor += n
	if err == io.EOF {
		return n, ErrPrematureEOF
	}
	if err != nil {
		return n, err
	}
	if r.filecursor == size {
		err = io.EOF
	}
	return n, err
}
