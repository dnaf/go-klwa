package klwa

import (
	"io"
)

func checkStringForNull(str string) bool {
	for i := 0; i < len(str); i++ {
		if str[i] == 0 {
			return true
		}
	}
	return false
}

func readNullTerminatedString(r io.Reader) (string, error) {
	buf := make([]byte, 0)
	b := make([]byte, 1)
	for {
		if _, err := r.Read(b); err != nil {
			if err == io.EOF {
				return "", ErrPrematureEOF
			}
			return "", err
		}
		if b[0] == 0 {
			break
		}
		buf = append(buf, b[0])
	}
	return string(buf), nil
}
