package main

import (
	"gitlab.com/dnaf/go-klwa/klwa/cmd"
)

func main() {
	cmd.Execute()
}
