module gitlab.com/dnaf/go-klwa/klwa

replace gitlab.com/dnaf/go-klwa => ../

go 1.13

require (
	github.com/spf13/cobra v0.0.5
	gitlab.com/dnaf/go-klwa v0.1.0
)
