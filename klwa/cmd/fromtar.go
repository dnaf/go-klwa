package cmd

import (
	"archive/tar"
	"fmt"
	"gitlab.com/dnaf/go-klwa"
	"io"
	"os"

	"github.com/spf13/cobra"
)

var fromtarCmd = &cobra.Command{
	Use:   "fromtar <tar>",
	Short: "converts a tar file to a klwa archive and writes to stdout",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			cmd.Usage()
			return
		}
		tarf, err := os.Open(args[0])
		if err != nil {
			panic(err)
		}

		// Get files
		files := make([]klwa.FileHeader, 0)
		{
			tr := tar.NewReader(tarf)
			for {
				hdr, err := tr.Next()
				if err == io.EOF {
					break
				}
				if err != nil {
					panic(err)
				}
				if hdr.Typeflag == tar.TypeReg {
					files = append(files, klwa.FileHeader{
						Name: hdr.Name,
						Size: int(hdr.Size),
					})
				}
			}
		}

		if _, err := tarf.Seek(0, 0); err != nil {
			panic(err)
		}

		// Write archive
		{
			tr := tar.NewReader(tarf)
			w, err := klwa.NewWriter(os.Stdout, files)
			if err != nil {
				panic(err)
			}

			for range files {
				klwaInfo, err := w.Next()
				if err != nil {
					panic(err)
				}
				fmt.Fprintf(os.Stderr, "%s\n", klwaInfo.Name)
				var tarInfo *tar.Header
				for {
					var err error
					tarInfo, err = tr.Next()
					if err != nil {
						panic(err)
					}
					if tarInfo.Typeflag == tar.TypeReg {
						break
					}
				}
				if klwaInfo.Name != tarInfo.Name || klwaInfo.Size != int(tarInfo.Size) {
					panic("archive seems to have changed while writing")
				}

				if _, err := io.CopyN(w, tr, tarInfo.Size); err != nil {
					panic(err)
				}
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(fromtarCmd)

	fromtarCmd.Flags().BoolP("verbose", "v", false, "be verbose")
}
