package cmd

import (
	"archive/tar"
	"fmt"
	"gitlab.com/dnaf/go-klwa"
	"io"
	"os"

	"github.com/spf13/cobra"
)

var totarCmd = &cobra.Command{
	Use:   "totar",
	Short: "converts a klwa archive from stdin to a tar file and writes to stdout",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 0 {
			cmd.Usage()
			return
		}

		kr, err := klwa.NewReader(os.Stdin)
		if err != nil {
			panic(err)
		}

		tw := tar.NewWriter(os.Stdout)
		defer tw.Close()

		for {
			hdr, err := kr.Next()
			if err == io.EOF {
				break
			}
			if err != nil {
				panic(err)
			}
			fmt.Fprintf(os.Stderr, "%s\n", hdr.Name)
			if err := tw.WriteHeader(&tar.Header{
				Typeflag: tar.TypeReg,
				Name:     hdr.Name,
				Mode:     0644,
				Size:     int64(hdr.Size),
			}); err != nil {
				panic(err)
			}
			if _, err := io.Copy(tw, kr); err != nil {
				panic(err)
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(totarCmd)

	totarCmd.Flags().BoolP("verbose", "v", false, "be verbose")
}
