package klwa

import (
	"errors"
)

var (
	ErrArchiveTooLarge = errors.New("klwa: archive too large")
	ErrFileTooBig      = errors.New("klwa: file too big")
	ErrNoFilesLeft     = errors.New("klwa: no files left")
	ErrNextNotCalled   = errors.New("klwa: Writer.Next not called before first Writer.Write or Reader.Read")
	ErrNullInFilename  = errors.New("klwa: filename contains null")
	ErrPrematureEOF    = errors.New("klwa: archive ended too soon")
	ErrReadTooLong     = errors.New("klwa: read too long")
	ErrWriteTooLong    = errors.New("klwa: write too long")
	ErrWriteTooShort   = errors.New("klwa: write too short")
)
