package klwa

import (
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"testing"
)

func chkHeader(t *testing.T, files, expected []FileHeader) {
	if len(files) != len(expected) {
		t.Errorf("expected %d files; got %d", len(expected), len(files))
	}
	for i := range files {
		if files[i] != expected[i] {
			t.Errorf("expected %v; got %v", expected[i], files[i])
		}
	}
}

func ExampleReader() {
	f, err := os.Open("testfiles/all.klwa")
	if err != nil {
		panic(err)
	}

	rd, err := NewReader(f)
	if err != nil {
		panic(err)
	}

	fmt.Print("files:")
	for _, hdr := range rd.Files {
		fmt.Printf(" %s (%d bytes),", hdr.Name, hdr.Size)
	}
	fmt.Println()
	fmt.Println()

	for {
		hdr, err := rd.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		fmt.Printf("== %q (%d bytes)\n", hdr.Name, hdr.Size)

		data, err := ioutil.ReadAll(rd)
		if err != nil {
			panic(err)
		}
		fmt.Println(hex.Dump(data))
	}

	// Output:
	// files: lorem.txt (57 bytes), test.png (724 bytes),
	//
	// == "lorem.txt" (57 bytes)
	// 00000000  4c 6f 72 65 6d 20 69 70  73 75 6d 20 64 6f 6c 6f  |Lorem ipsum dolo|
	// 00000010  72 20 73 69 74 20 61 6d  65 74 2c 20 63 6f 6e 73  |r sit amet, cons|
	// 00000020  65 63 74 65 74 75 72 20  61 64 69 70 69 73 63 69  |ectetur adipisci|
	// 00000030  6e 67 20 65 6c 69 74 2e  0a                       |ng elit..|
	//
	// == "test.png" (724 bytes)
	// 00000000  89 50 4e 47 0d 0a 1a 0a  00 00 00 0d 49 48 44 52  |.PNG........IHDR|
	// 00000010  00 00 01 f4 00 00 01 f4  04 03 00 00 00 39 f8 c2  |.............9..|
	// 00000020  b9 00 00 00 04 67 41 4d  41 00 00 b1 8f 0b fc 61  |.....gAMA......a|
	// 00000030  05 00 00 00 2a 50 4c 54  45 00 00 c0 00 21 4c 00  |....*PLTE....!L.|
	// 00000040  c0 00 00 c0 c0 09 09 09  13 13 13 1d 1d 1d 32 00  |..............2.|
	// 00000050  6a 97 ae 19 c0 00 00 c0  00 c0 c0 c0 00 c0 c0 c0  |j...............|
	// 00000060  ff ff ff d9 bb 0a 1a 00  00 02 55 49 44 41 54 78  |..........UIDATx|
	// 00000070  da ed d1 a1 11 c2 40 10  86 d1 b4 40 0b 94 40 5a  |......@....@..@Z|
	// 00000080  a0 85 20 32 58 e8 80 a1  00 1a c0 52 00 26 16 49  |.. 2X......R.&.I|
	// 00000090  0b 78 14 bd 20 89 db ff  64 86 f7 c9 13 3b fb 6e  |.x.. ...d....;.n|
	// 000000a0  bb 57 d0 b3 ee d1 d7 6d  d6 75 53 d0 ad ee da 05  |.W.....m.uS.....|
	// 000000b0  a1 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 000000c0  a3 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 000000d0  a3 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 000000e0  a3 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 000000f0  a3 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 00000100  a3 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 00000110  a3 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 00000120  a3 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 00000130  a3 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 00000140  a3 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 00000150  a3 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 00000160  a3 a3 a3 a3 a3 a3 a3 a3  a3 a3 a3 a3 a3 a3 a3 a3  |................|
	// 00000170  ff 0d fd e2 ea e8 e8 e8  e8 e8 e8 e8 e8 e8 e8 e8  |................|
	// 00000180  e8 e8 e8 e8 e8 e8 e8 e8  e8 e8 e8 e8 e8 e8 e8 e8  |................|
	// 00000190  e8 e8 e8 e8 e8 e8 e8 e8  e8 e8 e8 e8 e8 e8 e8 e8  |................|
	// 000001a0  e8 e8 e8 e8 e8 e8 e8 e8  e8 e8 e8 e8 e8 e8 e8 e8  |................|
	// 000001b0  e8 e8 e8 e8 e8 e8 e8 e8  e8 e8 e8 e8 e8 e8 e8 e8  |................|
	// 000001c0  e8 e8 e8 e8 e8 e8 e8 e8  e8 e8 e8 e8 e8 e8 e8 e8  |................|
	// 000001d0  e8 e8 e8 e8 e8 e8 e8 e8  e8 e8 e8 e8 e8 e8 e8 e8  |................|
	// 000001e0  e8 e8 e8 e8 e8 e8 e8 e8  e8 e8 e8 e8 e8 e8 e8 e8  |................|
	// 000001f0  e8 e8 ed f4 a4 a1 6e 0c  56 be 07 73 82 1f ec 83  |......n.V..s....|
	// 00000200  31 fb e0 a0 e8 e8 e8 e8  e8 e8 e8 e8 e8 e8 e8 e8  |1...............|
	// 00000210  e8 e8 e8 e8 e8 e8 e8 e8  e8 e8 e8 e8 e8 e8 e8 e8  |................|
	// 00000220  e8 e8 e8 e8 e8 e8 e8 e8  e8 e8 e8 e8 e8 e8 e8 e8  |................|
	// 00000230  e8 e8 8b a4 af 5a fa 34  f4 3e 37 74 1a 1a da ce  |.....Z.4.>7t....|
	// 00000240  9a 3d ef 8e bf 0e c1 18  74 74 74 74 74 74 74 74  |.=......tttttttt|
	// 00000250  74 74 74 74 74 74 74 74  74 74 74 74 74 74 74 74  |tttttttttttttttt|
	// 00000260  74 74 74 74 74 74 74 74  74 74 74 74 74 74 74 74  |tttttttttttttttt|
	// 00000270  74 74 74 74 74 74 74 74  74 74 74 74 74 74 74 74  |tttttttttttttttt|
	// 00000280  74 74 74 74 74 74 74 74  74 74 74 74 74 74 74 74  |tttttttttttttttt|
	// 00000290  74 74 74 74 74 74 74 74  74 74 74 74 74 74 74 74  |tttttttttttttttt|
	// 000002a0  74 74 74 74 74 74 74 74  74 74 74 74 74 74 74 74  |tttttttttttttttt|
	// 000002b0  74 74 74 74 74 74 74 74  74 74 74 74 f4 c5 d2 bf  |tttttttttttt....|
	// 000002c0  66 56 5d bd c7 da 92 09  00 00 00 00 49 45 4e 44  |fV].........IEND|
	// 000002d0  ae 42 60 82                                       |.B`.|
}

func ExampleReader_oneFile() {
	f, err := os.Open("testfiles/all.klwa")
	if err != nil {
		panic(err)
	}

	rd, err := NewReader(f)
	if err != nil {
		panic(err)
	}

	fmt.Print("files:")
	for _, hdr := range rd.Files {
		fmt.Printf(" %s (%d bytes),", hdr.Name, hdr.Size)
	}
	fmt.Println()

	var lorem string
	for {
		// check next file
		hdr, err := rd.Next()

		if err == io.EOF { // iterated over all files
			panic("lorem.txt not found")
		}
		if err != nil {
			panic(err)
		}

		// see if its the one we want
		if hdr.Name == "lorem.txt" {
			// read file
			buf, err := ioutil.ReadAll(rd)
			if err != nil {
				panic(err)
			}
			// set lorem and escape loop
			lorem = string(buf)
			break
		}
	}
	fmt.Printf("lorem.txt: %q\n", lorem)

	// Output:
	// files: lorem.txt (57 bytes), test.png (724 bytes),
	// lorem.txt: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n"
}

func TestReader_NonexistentFile(t *testing.T) {
	f, err := os.Open("testfiles/nonexistentfile.klwa")
	if err != nil {
		panic(err)
	}

	rd, err := NewReader(f)
	if err != nil {
		panic(err)
	}

	chkHeader(t, rd.Files, []FileHeader{
		FileHeader{"existent", 42, 0},
		FileHeader{"nonexistent", 255, 0},
	})

	// First file should be read clearly
	{
		expected := "this file exists! the other one doesn't :/"
		hdr, err := rd.Next()
		if err != nil {
			panic(err)
		}
		buf, err := ioutil.ReadAll(rd)
		if err != nil {
			panic(err)
		}
		if string(buf) != expected {
			t.Errorf("file %q != %q; instead got %q", hdr.Name, expected, string(buf))
		}
	}

	// Second file should return an error
	{
		hdr, err := rd.Next()
		if err != nil {
			panic(err)
		}
		buf := make([]byte, 1)
		_, err = rd.Read(buf)
		if err != ErrPrematureEOF {
			t.Errorf("first read for %q was expected to return an ErrPrematureEOF but it did not", hdr.Name)
		}
	}
}

func TestReader_NonTerminatingHeader(t *testing.T) {
	f, err := os.Open("testfiles/nonterminatingheader.klwa")
	if err != nil {
		panic(err)
	}

	_, err = NewReader(f)
	if err != ErrPrematureEOF {
		t.Error("reading header was expected to return an ErrPrematureEOF but it did not")
	}
}

func TestReader_OutOfBounds(t *testing.T) {
	f, err := os.Open("testfiles/outofbounds.klwa")
	if err != nil {
		panic(err)
	}

	rd, err := NewReader(f)
	if err != nil {
		panic(err)
	}

	chkHeader(t, rd.Files, []FileHeader{
		FileHeader{"out of bounds", 0x80, 0},
	})

	if _, err := rd.Next(); err != nil {
		panic(err)
	}

	_, err = ioutil.ReadAll(rd)
	if err != ErrPrematureEOF {
		t.Error("reading file was expected to return an ErrPrematureEOF but it did not")
	}
}

func TestReader_NextNotCalled(t *testing.T) {
	f, err := os.Open("testfiles/all.klwa")
	if err != nil {
		panic(err)
	}

	rd, err := NewReader(f)
	if err != nil {
		panic(err)
	}

	_, err = ioutil.ReadAll(rd)
	if err != ErrNextNotCalled {
		t.Error("reading file was expected to return an ErrNextNotCalled but it did not")
	}
}
