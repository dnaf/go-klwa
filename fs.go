package klwa

import (
	"errors"
	"io"
	"net/http"
	"os"
	"sync"
	"time"
)

type fsFileIndex struct {
	Size, Offset int
}

type FileSystem struct {
	r   io.ReadSeeker
	rmx sync.Mutex

	ra io.ReaderAt

	headerSize int

	files map[string]*fsFileIndex
}

func NewFileSystem(r io.ReadSeeker) (http.FileSystem, error) {
	fs := &FileSystem{
		r:     r,
		files: make(map[string]*fsFileIndex, 0),
	}
	if ra, ok := r.(io.ReaderAt); ok {
		fs.ra = ra
	}

	for {
		hdr, err := readFileHeader(fs.r)
		if err != nil {
			return nil, err
		}
		if hdr == nil {
			break
		}
		file := fsFileIndex{
			Size: hdr.Size,
		}
		fs.files[hdr.Name] = &file
	}

	{
		pos, err := fs.r.Seek(0, io.SeekCurrent)
		if err != nil {
			return nil, err
		}
		fs.headerSize = int(pos)
	}

	{
		var acc = fs.headerSize
		for _, file := range fs.files {
			file.Offset = acc
			acc += file.Size
		}
	}

	return fs, nil
}

func (fs *FileSystem) readAt(b []byte, off int64) (int, error) {
	if fs.ra != nil {
		return fs.ra.ReadAt(b, off)
	}
	fs.rmx.Lock()
	defer fs.rmx.Unlock()
	if _, err := fs.r.Seek(off, io.SeekStart); err != nil {
		return 0, err
	}
	return fs.r.Read(b)
}

type fsFile struct {
	fs     *FileSystem
	file   bool // if true, file is an actual file. otherwise is a "directory"
	name   string
	offset int
	size   int

	cursor int
}

func (fs *FileSystem) Open(name string) (http.File, error) {
	idx, ok := fs.files[name]
	if !ok {
		return nil, os.ErrNotExist
	}
	return &fsFile{
		fs:     fs,
		file:   true,
		name:   name,
		offset: idx.Offset,
		size:   idx.Size,
	}, nil
}

func (f *fsFile) Close() error {
	return nil
}

func (f *fsFile) Read(p []byte) (int, error) {
	maxn := len(p)
	if f.cursor+len(p) > f.size {
		maxn = f.size - f.cursor
	}
	n, err := f.fs.readAt(p[:maxn], int64(f.cursor+f.offset))
	f.cursor += n
	if err != nil {
		return n, err
	}
	if f.cursor == f.size {
		err = io.EOF
	}
	return n, err
}

func (f *fsFile) Seek(offset int64, whence int) (int64, error) {
	switch whence {
	case io.SeekCurrent:
		offset = int64(f.cursor) + offset
	case io.SeekEnd:
		offset = int64(f.size) + offset
	}
	if offset < 0 || offset > int64(f.size) {
		return offset, errors.New("out of bounds seek")
	}
	return offset, nil
}

func (f *fsFile) Readdir(count int) ([]os.FileInfo, error) {
	panic("not implemented")
}

func (f *fsFile) Stat() (os.FileInfo, error) {
	return f, nil
}

func (f *fsFile) Name() string {
	return f.name
}
func (f *fsFile) Size() int64 {
	return int64(f.size)
}
func (f *fsFile) Mode() (mode os.FileMode) {
	if f.file {
		mode = 0644
	} else {
		mode = 0755 | os.ModeDir
	}
	return
}
func (f *fsFile) ModTime() time.Time {
	return time.Unix(0, 0)
}
func (f *fsFile) IsDir() bool {
	return !f.file
}
func (f *fsFile) Sys() interface{} {
	return nil
}
